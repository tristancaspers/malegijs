package com.tristancaspers.malegijs.activity.database;

public class DatabaseInfo {
    public class MalegijsTables {
        public static final String FRUITLIJN = "fruitlijn";
        public static final String DRINKLAND = "drinkland";
    }

    public class FruitlijnColumn {
        public static final String ID = "id";
        public static final String SOORT = "soort";
        public static final String AANTAL = "aantal";
        public static final String FORMAAT = "formaat";
    }

    public class DrinklandColumn {
        public static final String ID = "id";
        public static final String SOORT = "soort";
        public static final String AANTAL = "aantal";
        public static final String FORMAAT = "formaat";
    }
}
