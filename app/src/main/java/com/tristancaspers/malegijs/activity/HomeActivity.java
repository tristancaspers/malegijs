package com.tristancaspers.malegijs.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.model.Bedrijf;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.mCustomToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("    MALEGIJS");
        getSupportActionBar().setIcon(getDrawable(R.drawable.ic_local_dining_black_24dp));
    }

    public void h_buttons (View view) {
        if (findViewById(R.id.knopFruitlijn).isPressed())
            startActivity(new Intent(getApplicationContext(), FruitlijnActivity.class));
        else if (findViewById(R.id.knopFruitlijnChart).isPressed()) {
            Intent i = new Intent(getApplicationContext(), OrderChartActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("bedrijf", Bedrijf.FRUITLIJN);
            i.putExtras(b);
            startActivity(i);
        }
        else if (findViewById(R.id.knopDrinkland).isPressed())
            startActivity(new Intent(getApplicationContext(), DrinklandActivity.class));
        else if (findViewById(R.id.knopDrinklandChart).isPressed()) {
            Intent i = new Intent(getApplicationContext(), OrderChartActivity.class);
            Bundle b = new Bundle();
            b.putSerializable("bedrijf", Bedrijf.DRINKLAND);
            i.putExtras(b);
            startActivity(i);
        }
    }
}