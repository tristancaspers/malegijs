package com.tristancaspers.malegijs.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.database.DatabaseHelper;
import com.tristancaspers.malegijs.activity.model.Bedrijf;

import java.util.ArrayList;

public class OrderChartActivity extends AppCompatActivity {

    private PieChart mChart;
    private PieChart mChart2;
    private PieChart mChart3;
    private Bedrijf b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_chart);

        Bundle bundle = getIntent().getExtras();
        b = (Bedrijf) bundle.getSerializable("bedrijf");

        LinearLayout mLayout = (LinearLayout) findViewById(R.id.activity_order_chart);
        if (b == Bedrijf.FRUITLIJN)
            mLayout.setBackgroundResource(R.color.fruitlijnColor);
        else if (b == Bedrijf.DRINKLAND)
            mLayout.setBackgroundResource(R.color.drinklandColor);

        mChart = (PieChart) findViewById(R.id.OrderPieChart);
        mChart.setDescription("Stuk / Fles");
        mChart.setTouchEnabled(true);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleRadius(0);
        mChart.setHoleRadius(40);
        mChart.animateY(1000, Easing.EasingOption.EaseInOutQuad);
        mChart.setNoDataText("");

        mChart2 = (PieChart) findViewById(R.id.OrderPieChart2);
        mChart2.setDescription("Doos / Tree / Post mix");
        mChart2.setTouchEnabled(true);
        mChart2.setDrawSliceText(true);
        mChart2.getLegend().setEnabled(false);
        mChart2.setTransparentCircleRadius(0);
        mChart2.setHoleRadius(40);
        mChart2.animateY(1000, Easing.EasingOption.EaseInOutQuad);
        mChart2.setNoDataText("");

        mChart3 = (PieChart) findViewById(R.id.OrderPieChart3);
        mChart3.setDescription("Kist / Krat / Fust");
        mChart3.setTouchEnabled(true);
        mChart3.setDrawSliceText(true);
        mChart3.getLegend().setEnabled(false);
        mChart3.setTransparentCircleRadius(0);
        mChart3.setHoleRadius(40);
        mChart3.animateY(1000, Easing.EasingOption.EaseInOutQuad);
        mChart3.setNoDataText("");

        setData();
    }

    private void setData() {
        ArrayList<Entry> yValues = new ArrayList<>();
        ArrayList<String> xValues = new ArrayList<>();
        ArrayList<Entry> yValues2 = new ArrayList<>();
        ArrayList<String> xValues2 = new ArrayList<>();
        ArrayList<Entry> yValues3 = new ArrayList<>();
        ArrayList<String> xValues3 = new ArrayList<>();
        DatabaseHelper dbHelper = DatabaseHelper.getHelper(this);
        Cursor rs = dbHelper.readListItems(b);
        int i = 0;
        while(rs.moveToNext()) {
            if (!rs.getString(2).isEmpty()) {
                if (rs.getString(3).equals("stuk") || rs.getString(3).equals("fles")) {
                    yValues.add(new Entry(Integer.parseInt(rs.getString(2)), i));
                    xValues.add(rs.getString(1));
                } else if (rs.getString(3).equals("doos") || rs.getString(3).equals("tree") || rs.getString(3).equals("post mix")) {
                    yValues2.add(new Entry(Integer.parseInt(rs.getString(2)), i));
                    xValues2.add(rs.getString(1));
                } else if (rs.getString(3).equals("kist") || rs.getString(3).equals("krat") || rs.getString(3).equals("fust")) {
                    yValues3.add(new Entry(Integer.parseInt(rs.getString(2)), i));
                    xValues3.add(rs.getString(1));
                }
            }
            i++;
        }

        PieDataSet ds = new PieDataSet(yValues, "");
        PieDataSet ds2 = new PieDataSet(yValues2, "");
        PieDataSet ds3 = new PieDataSet(yValues3, "");

        ds.setColors(ColorTemplate.JOYFUL_COLORS);
        ds2.setColors(ColorTemplate.JOYFUL_COLORS);
        ds3.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData d = new PieData(xValues, ds);
        PieData d2 = new PieData(xValues2, ds2);
        PieData d3 = new PieData(xValues3, ds3);

        mChart.setData(d);
        mChart2.setData(d2);
        mChart3.setData(d3);

        mChart.invalidate();
        mChart2.invalidate();
        mChart3.invalidate();
    }
}
