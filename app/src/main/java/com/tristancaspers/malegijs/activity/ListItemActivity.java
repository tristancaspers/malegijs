package com.tristancaspers.malegijs.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.database.DatabaseHelper;
import com.tristancaspers.malegijs.activity.model.Bedrijf;
import com.tristancaspers.malegijs.activity.model.ListItem;

public class ListItemActivity extends AppCompatActivity {

    private DatabaseHelper dbHelper;
    private Bedrijf b;
    private EditText mInputSoort;
    private EditText mInputAantal;
    private Spinner mFormaatSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_item);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.mCustomToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("    MALEGIJS");
        getSupportActionBar().setIcon(getDrawable(R.drawable.ic_local_dining_black_24dp));

        dbHelper = DatabaseHelper.getHelper(this);
        Bundle bundle = getIntent().getExtras();
        b = (Bedrijf) bundle.getSerializable("bedrijf");

        RelativeLayout mLayout = (RelativeLayout) findViewById(R.id.activity_listitem);
        mInputSoort = (EditText) findViewById(R.id.inputSoort);
        mInputAantal = (EditText) findViewById(R.id.inputAantal);
        mFormaatSpinner = (Spinner) findViewById(R.id.formaatSpinner);

        ArrayAdapter<String> spinnerAdapter = null;
        if (b == Bedrijf.FRUITLIJN) {
            mLayout.setBackgroundResource(R.color.fruitlijnColor);
            spinnerAdapter = new ArrayAdapter<>(ListItemActivity.this, android.R.layout.simple_list_item_activated_1, getResources().getStringArray(R.array.fruitlijn_formaten));
        } else if (b == Bedrijf.DRINKLAND) {
            mLayout.setBackgroundResource(R.color.drinklandColor);
            spinnerAdapter = new ArrayAdapter<>(ListItemActivity.this, android.R.layout.simple_list_item_activated_1, getResources().getStringArray(R.array.drinkland_formaten));
        }
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFormaatSpinner.setPrompt("Selecteer het formaat:");
        mFormaatSpinner.setAdapter(spinnerAdapter);
    }

    public void l_buttons(View view) {
        if (findViewById(R.id.knopVoegItemToe).isPressed()) {
            toevoegenItem();
        }
    }

    public void toevoegenItem() {
        String soort = mInputSoort.getText().toString();
        String aantal = mInputAantal.getText().toString();
        String formaat = mFormaatSpinner.getSelectedItem().toString();
        if (soort.isEmpty() || aantal.isEmpty())
            toonSnackbar("VUL EERST BEIDE VELDEN IN");
        else {
            String cSoort = soort.substring(0, 1).toUpperCase() + soort.substring(1).toLowerCase();
            dbHelper.createListItem(new ListItem(cSoort, aantal, formaat), b);
            if (b == Bedrijf.FRUITLIJN )
                startActivity(new Intent(getApplicationContext(), FruitlijnActivity.class));
            else if (b == Bedrijf.DRINKLAND)
                startActivity(new Intent(getApplicationContext(), DrinklandActivity.class));
            finish();
        }
    }

    public void toonSnackbar(String bericht) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), bericht, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        TextView tv = view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snack.show();
    }
}