package com.tristancaspers.malegijs.activity.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.tristancaspers.malegijs.activity.model.Bedrijf;
import com.tristancaspers.malegijs.activity.model.ListItem;

import static com.tristancaspers.malegijs.activity.database.DatabaseInfo.MalegijsTables.DRINKLAND;
import static com.tristancaspers.malegijs.activity.database.DatabaseInfo.MalegijsTables.FRUITLIJN;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static SQLiteDatabase mDB;
    private static DatabaseHelper mInstance;

    private DatabaseHelper(Context ctx) {
        super(ctx, "Malegijs.db", null, 1);
    }

    public static synchronized DatabaseHelper getHelper (Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx);
            mDB = mInstance.getWritableDatabase();
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + FRUITLIJN + " (" +
                DatabaseInfo.FruitlijnColumn.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.FruitlijnColumn.SOORT + " TEXT, " +
                DatabaseInfo.FruitlijnColumn.AANTAL + " TEXT, " +
                DatabaseInfo.FruitlijnColumn.FORMAAT + " TEXT);"
        );

        db.execSQL("CREATE TABLE " + DRINKLAND + " (" +
                DatabaseInfo.DrinklandColumn.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                DatabaseInfo.DrinklandColumn.SOORT + " TEXT, " +
                DatabaseInfo.DrinklandColumn.AANTAL + " TEXT, " +
                DatabaseInfo.DrinklandColumn.FORMAAT + " TEXT);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + FRUITLIJN + ";");
        db.execSQL("DROP TABLE IF EXISTS " + DRINKLAND + ";");
        onCreate(db);
    }

    public void createListItem (ListItem li, Bedrijf b) {
        ContentValues values = new ContentValues();
        if (b == Bedrijf.FRUITLIJN) {
            values.put(DatabaseInfo.FruitlijnColumn.SOORT, li.getSoort());
            values.put(DatabaseInfo.FruitlijnColumn.AANTAL, li.getAantal());
            values.put(DatabaseInfo.FruitlijnColumn.FORMAAT, li.getFormaat());
            mDB.insert(DatabaseInfo.MalegijsTables.FRUITLIJN, null, values);
        } else if (b == Bedrijf.DRINKLAND) {
            values.put(DatabaseInfo.DrinklandColumn.SOORT, li.getSoort());
            values.put(DatabaseInfo.DrinklandColumn.AANTAL, li.getAantal());
            values.put(DatabaseInfo.DrinklandColumn.FORMAAT, li.getFormaat());
            mDB.insert(DatabaseInfo.MalegijsTables.DRINKLAND, null, values);
        }
    }

    public Cursor readListItems (Bedrijf b) {
        if (b == Bedrijf.FRUITLIJN)
            return mDB.rawQuery("SELECT * FROM " + DatabaseInfo.MalegijsTables.FRUITLIJN + ";", null);
        else if (b == Bedrijf.DRINKLAND)
            return mDB.rawQuery("SELECT * FROM " + DatabaseInfo.MalegijsTables.DRINKLAND + ";", null);
        else
            return null;
    }

    public void updateListItem (ListItem li, Bedrijf b) {
        if (b == Bedrijf.FRUITLIJN) {
            mDB.execSQL("UPDATE " + DatabaseInfo.MalegijsTables.FRUITLIJN + " SET " +
                    DatabaseInfo.FruitlijnColumn.AANTAL + " = '" + li.getAantal() + "' WHERE " +
                    DatabaseInfo.FruitlijnColumn.SOORT + " = '" + li.getSoort() + "';");
        } else if (b == Bedrijf.DRINKLAND) {
            mDB.execSQL("UPDATE " + DatabaseInfo.MalegijsTables.DRINKLAND + " SET " +
                    DatabaseInfo.DrinklandColumn.AANTAL + " = '" + li.getAantal() + "' WHERE " +
                    DatabaseInfo.DrinklandColumn.SOORT + " = '" + li.getSoort() + "';");
        }
    }

    public void updateListItemsToZero(Bedrijf b) {
        if (b == Bedrijf.FRUITLIJN) {
            mDB.execSQL("UPDATE " + DatabaseInfo.MalegijsTables.FRUITLIJN + " SET " + DatabaseInfo.FruitlijnColumn.AANTAL + " = '';");
        } else if (b == Bedrijf.DRINKLAND) {
            mDB.execSQL("UPDATE " + DatabaseInfo.MalegijsTables.DRINKLAND + " SET " + DatabaseInfo.DrinklandColumn.AANTAL + " = '';");
        }
    }

    public void deleteListItem (ListItem li, Bedrijf b) {
        if (b == Bedrijf.FRUITLIJN) {
            mDB.execSQL("DELETE FROM " + DatabaseInfo.MalegijsTables.FRUITLIJN + " WHERE " +
                    DatabaseInfo.FruitlijnColumn.SOORT + " = '" + li.getSoort() + "';");
        } else if (b == Bedrijf.DRINKLAND) {
            mDB.execSQL("DELETE FROM " + DatabaseInfo.MalegijsTables.DRINKLAND + " WHERE " +
                    DatabaseInfo.DrinklandColumn.SOORT + " = '" + li.getSoort() + "';");
        }
    }
}
