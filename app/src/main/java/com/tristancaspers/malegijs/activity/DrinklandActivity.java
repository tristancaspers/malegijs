package com.tristancaspers.malegijs.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.database.DatabaseHelper;
import com.tristancaspers.malegijs.activity.database.DatabaseInfo;
import com.tristancaspers.malegijs.activity.model.Bedrijf;
import com.tristancaspers.malegijs.activity.model.ListItem;
import com.tristancaspers.malegijs.activity.model.ListItemAdapter;
import com.tristancaspers.malegijs.activity.service.Api;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DrinklandActivity extends AppCompatActivity {

    private DatabaseHelper dbHelper;
    private Bedrijf bedrijf = Bedrijf.DRINKLAND;
    private String bestelling = "";
    List<ListItem> mListItems = new ArrayList<ListItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drinkland);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.mCustomToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("    MALEGIJS");
        getSupportActionBar().setIcon(getDrawable(R.drawable.ic_local_dining_black_24dp));

        dbHelper = DatabaseHelper.getHelper(this);
        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ListItemAdapter mAdapter = new ListItemAdapter(mListItems, this, bedrijf);
        mRecyclerView.setAdapter(mAdapter);

        Cursor rs = dbHelper.getWritableDatabase().rawQuery("SELECT COUNT(*) FROM " + DatabaseInfo.MalegijsTables.DRINKLAND, null);
        rs.moveToFirst();
        int rows = rs.getInt(0);
        rs.close();
        if(rows <= 0) {
            laadTemplate();
        }
        toonLijst();
    }

    public void d_buttons (View view) {
        if (findViewById(R.id.knopResetLijst).isPressed()) {
            resetLijst();
        } else if (findViewById(R.id.knopAddListItem).isPressed()) {
            nieuwArtikel();
        } else if (findViewById(R.id.knopMailen).isPressed()) {
            mailLijst();
        }
    }

    public void toonLijst() {
        Cursor rs = dbHelper.readListItems(bedrijf);
        while(rs.moveToNext()) {
            mListItems.add(new ListItem(rs.getString(1), rs.getString(2), rs.getString(3)));
        }
    }

    public void resetLijst() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("").setMessage("Alle ingevoerde getallen verwijderen?").setPositiveButton("JA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dbHelper.updateListItemsToZero(bedrijf);
                finish();
                startActivity(getIntent());
            }
        }).setNegativeButton("NEE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }

    public void nieuwArtikel() {
        Intent i = new Intent(getApplicationContext(), ListItemActivity.class);
        Bundle b = new Bundle();
        b.putSerializable("bedrijf", bedrijf);
        i.putExtras(b);
        startActivity(i);
        finish();
    }

    public void mailLijst() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Opmerking toevoegen?");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!input.getText().toString().isEmpty())
                    bestelling = input.getText().toString() + "\n\n";
                for(int i = 0; i < mListItems.size(); i++) {
                    if (!mListItems.get(i).getAantal().isEmpty())
                        bestelling += mListItems.get(i).toString();
                }
                bestelling += "\nGroetjes,\nHans Caspers";

                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                String [] mailto = {"tristancaspers97@hotmail.com"};
                i.putExtra(Intent.EXTRA_EMAIL, mailto);
                i.putExtra(Intent.EXTRA_SUBJECT, "Malegijs Bestelling");
                i.putExtra(Intent.EXTRA_TEXT, bestelling);
                bestelling = "";
                try {
                    startActivityForResult(Intent.createChooser(i, "Kies een app om naar te verzenden"), 1000);
                } catch (android.content.ActivityNotFoundException ex) {
                    toonSnackbar("GEEN EMAIL APPS GEÏNSTALLEERD");
                }
            }
        });
        builder.setNegativeButton("TERUG", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void laadTemplate() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api api = retrofit.create(Api.class);
        Call<List<ListItem>> call = api.getDrinkland();
        call.enqueue(new Callback<List<ListItem>>() {
            @Override
            public void onResponse(Call<List<ListItem>> call, Response<List<ListItem>> response) {
                List<ListItem> listitems = response.body();
                for(ListItem listitem : listitems) {
                    String soort = listitem.getSoort();
                    String aantal = listitem.getAantal();
                    String formaat = listitem.getFormaat();
                    dbHelper.createListItem(new ListItem(soort, aantal, formaat), bedrijf);
                }
                finish();
                startActivity(new Intent(getApplicationContext(), DrinklandActivity.class));
            }
            @Override
            public void onFailure(Call<List<ListItem>> call, Throwable t) {
                toonSnackbar("Data is onbereikbaar, controleer netwerk..");
            }
        });
    }

    public void toonSnackbar(String bericht) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), bericht, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        TextView tv = view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        snack.show();
    }
}
