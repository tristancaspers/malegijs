package com.tristancaspers.malegijs.activity.model;

public class ListItem {

    private String soort;
    private String aantal;
    private String formaat;

    public ListItem(String soort, String aantal, String formaat) {
        this.soort = soort;
        this.aantal = aantal;
        this.formaat = formaat;
    }

    public String getSoort() {
        return soort;
    }

    public void setSoort(String soort) {
        this.soort = soort;
    }

    public String getAantal() {
        return aantal;
    }

    public void setAantal(String aantal) {
        this.aantal = aantal;
    }

    public String getFormaat() {
        return formaat;
    }

    public void setFormaat(String formaat) {
        this.formaat = formaat;
    }

    @Override
    public String toString() {
        return soort + " - " + aantal + " (" + formaat + ")\n";
    }
}