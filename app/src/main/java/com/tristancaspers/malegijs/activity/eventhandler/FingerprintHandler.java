package com.tristancaspers.malegijs.activity.eventhandler;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.HomeActivity;

public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context mContext;

    public FingerprintHandler(Context context) {
        mContext = context;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("" + errString, false);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("" + helpString, false);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("TOEGANG GEWEIGERD", false);
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("TOEGANG VERLEEND", true);
        Activity ac = (Activity) mContext;
        ac.finish();
        ac.startActivity(new Intent(ac, HomeActivity.class));
    }

    private void update(String e, Boolean success){
        TextView mToegangsMelding = ((Activity) mContext).findViewById(R.id.toegangsMelding);
        mToegangsMelding.setText(e);
        if(success){
            mToegangsMelding.setTextColor(ContextCompat.getColor(mContext, R.color.succeedText));
        }
    }
}
