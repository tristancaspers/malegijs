package com.tristancaspers.malegijs.activity.service;

import com.tristancaspers.malegijs.activity.model.ListItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Api {
    // The free NoSQL database from Firebase is used here
    String ROOT_URL = "https://malegijs-b7a71.firebaseio.com/";

    @GET("fruitlijn.json")
    Call<List<ListItem>> getFruitlijn();

    @GET("drinkland.json")
    Call<List<ListItem>> getDrinkland();
}
