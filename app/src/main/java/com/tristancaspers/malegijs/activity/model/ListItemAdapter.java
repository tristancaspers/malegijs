package com.tristancaspers.malegijs.activity.model;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tristancaspers.malegijs.R;
import com.tristancaspers.malegijs.activity.DrinklandActivity;
import com.tristancaspers.malegijs.activity.FruitlijnActivity;
import com.tristancaspers.malegijs.activity.database.DatabaseHelper;

import java.util.List;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.ViewHolder> {

    private List<ListItem> mListItems;
    private Context mContext;
    private Bedrijf bedrijf;

    public ListItemAdapter(List<ListItem> mListItems, Context mContext, Bedrijf bedrijf) {
        this.mListItems = mListItems;
        this.mContext = mContext;
        this.bedrijf = bedrijf;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ListItem listItem = mListItems.get(position);

        holder.setIsRecyclable(false);
        holder.mSoortListItem.setText(listItem.getSoort());
        holder.mAantalListItem.setText(listItem.getAantal());
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.mAantalListItem.setFocusableInTouchMode(true);
                holder.mAantalListItem.requestFocus();
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
            }
        });

        holder.mLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                alertDialogBuilder.setTitle("").setMessage(listItem.getSoort() + " (" + listItem.getFormaat() + ") uit de standaard lijst verwijderen?").setPositiveButton("JA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseHelper.getHelper(mContext).deleteListItem(listItem, bedrijf);
                        Activity ac = (Activity) mContext;
                        ac.finish();
                        if (bedrijf == Bedrijf.FRUITLIJN)
                            ac.startActivity(new Intent(ac, FruitlijnActivity.class));
                        else if (bedrijf == Bedrijf.DRINKLAND)
                            ac.startActivity(new Intent(ac, DrinklandActivity.class));
                    }
                }).setNegativeButton("NEE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // SHOW MESSSAGE
                    }
                }).show();
                return true;
            }
        });

        holder.mAantalListItem.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                listItem.setAantal(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                DatabaseHelper.getHelper(mContext).updateListItem(listItem, bedrijf);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mSoortListItem;
        private EditText mAantalListItem;
        private RelativeLayout mLayout;

        private ViewHolder(View itemView) {
            super(itemView);

            mSoortListItem = itemView.findViewById(R.id.soortListItem);
            mAantalListItem = itemView.findViewById(R.id.aantalListItem);
            mLayout = itemView.findViewById(R.id.card_view_child_layout);
        }
    }
}
